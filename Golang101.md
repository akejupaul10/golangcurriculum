# Golang Curriculum: from Beginner to Pro

Learn the basics of Golang. It is advisable to Read through the book twice before starting to code them out. Its the most useful method ever i found out for learning. You could be a little bit bored but you are helping yourself to register these concepts in your head.
## PATH 1:Learning

#### Head First Go{2 times}
 ##### Understand  the following concept:
 - Datatypes <br>
 
 - <b>Arrays
 - <b>Slice
 - <b>Looping
 - <b>Maps
 - <b>Function
 - <b>Struct <br> Study the Example here and practise(5 times) : https://gobyexample.com/structs
 <br> Understanding Struct on an advanced level: https://www.golangprograms.com/go-language/struct.html <br>
 Struct: https://www.golang-book.com/books/intro/9 <br>
 Struct: http://zetcode.com/golang/struct/
 
 - <b>Defined types: <br>
    Types: https://golangbyexample.com/user-defined-function-type-go/ <br>
    Types: https://appdividend.com/2019/03/22/go-custom-type-declarations-tutorial-with-example/ <br>
    Types: https://golang.objectbox.io/custom-types
 - <b>Encapsulation <br> 
    1. https://www.geeksforgeeks.org/encapsulation-in-golang/
    2. https://golangbyexample.com/encapsulation-in-go/
    
 - <b>Interfaces <br>
    1. https://gobyexample.com/interfaces
    2. https://www.geeksforgeeks.org/interfaces-in-golang/

 - Goroutines & Channels <br>
    1. https://gobyexample.com/goroutines
    2. https://golangbot.com/goroutines/
    3. https://medium.com/technofunnel/understanding-golang-and-goroutines-72ac3c9a014d
    4. https://www.golang-book.com/books/intro/10
    5. https://www.geeksforgeeks.org/goroutines-concurrency-in-golang/